#!/usr/bin/env perl

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use warnings;

use constant BPM_HI       => 300;
use constant BPM_LOW      => 1;
use constant BPB_HI       => 32;
use constant BPB_LOW      => 2;
use constant MINS_HI      => 1000;
use constant MINS_LOW     => 1;
use constant MINS_DEFAULT => 5;

sub usage {
    die("usage: $0 BPM BPB [MINS]\n");
}

sub bar_timestamps {
    my ($bpm, $bpb, $mins) = @_;

    my $seconds_per_beat = 60 / $bpm;
    my $seconds_per_bar = $seconds_per_beat * $bpb;
    my $seconds_total = $mins * 60;

    my @t;
    for (my $s = 0; $s < $seconds_total; $s += $seconds_per_bar) {
        push @t, $s;
    }
    return @t;
}

MAIN: {
    my $argc = scalar(@ARGV);
    if ($argc != 2 && $argc != 3) {
        warn("wrong number of args\n");
        usage();
    }
    for (@ARGV) {
        if ($_ =~ m/\D/) {
            warn("argument is not a number: '$_'\n");
            usage();
        }
    }
    my ($bpm, $bpb, $mins) = @ARGV;
    $mins ||= MINS_DEFAULT;

    if ($bpm > BPM_HI) {
        warn("bpm too high\n");
        $bpm = BPM_HI;
    } elsif ($bpm < BPM_LOW) {
        warn("bpm too low\n");
        $bpm = BPM_LOW;
    }

    if ($bpb > BPB_HI) {
        warn("bpb too high\n");
        $bpb = BPB_HI;
    } elsif ($bpb < BPB_LOW) {
        warn("bpb too low\n");
        $bpb = BPB_LOW;
    }

    if ($mins > MINS_HI) {
        warn("minutes too high\n");
        $mins = MINS_HI;
    } elsif ($mins < MINS_LOW) {
        warn("minutes too low\n");
        $mins = MINS_LOW;
    }

    my $i = 0;
    for (bar_timestamps($bpm, $bpb, $mins)) {
        printf("%.6f\t%.6f\t%s\n", $_, $_, ++$i);
    }
    exit(0);
}

__END__

=pod

=head1 NAME

audacity_bar - generate label file for a song

=head1 SYNOPSIS

    audacity_bar BPM BPB [MINS]

=head1 DESCRIPTION

Generally DAW software supports visually dividing
a song into musical bars. Audacity does not have
this feature. To work around this limitation
Audacity supports so-called Label Tracks, which
are a set of Labels. A Label is simply a named
time offset in a song.

Label Tracks can be imported from a file.
The file format is text so it is easy to
generate. This tool creates a Label Track
file given a song's tempo information.

All arguments are numbers.
The first argument, BPM, is the number of beats
per minute. Next, BPB is beats per bar. For example,
a song with a time signature of 3/4 would have
BPB=3. The MINS argument is the number of minutes
in total the label information should cover.
If MINS is not provided a default of 5 minutes is
used.

=head1 BUGS

Fractional BPM values are not supported.

=head1 SEE ALSO

See Audacity software manual for a description
of Label Tracks.

=cut
