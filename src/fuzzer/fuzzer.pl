#!/usr/bin/env perl

# Copyright (c) 2019 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use bytes;
use integer;
use strict;
use warnings;

use IO::File qw();
use IPC::Open3 qw(open3);
use MIME::Base64 qw(encode_base64);

use Const::Fast qw(const);
use Unix::OpenBSD::Random qw(arc4random_buf);
use Unix::Pledge qw(pledge);

const my $BUFSZ => 1024;
const my $SIGEXIT_MIN => 126;
const my $SIGEXIT_MAX => 165;
const my $LOGNAME => 'fuzzer.log';

my $log;

sub fatal {
	my $text = 'ERROR: ' . shift() . "\n";
	print $text;
	$log->print($text) if ($log);
	exit(1);
}

sub get_random_buffer {
	my $buf = arc4random_buf($BUFSZ);
	fatal('arc4random_buf') if (length($buf) != $BUFSZ);
	return $buf;
}

sub crash_occurred {
	return 1 if ($? >= $SIGEXIT_MIN && $? <= $SIGEXIT_MAX);
	my @cores = glob('*.core');
	return (scalar(@cores) > 0) ? 1 : 0;
}

sub main_loop {
	my (@cmd) = @_;

	my $str = get_random_buffer();
	my($in, $out);
	my $pid = open3($in, $out, 0, @cmd);
	print {$in} $str;
	close($in);
	close($out);
	wait();
	print scalar(localtime) . ": child $pid exited with status ($?)\n";
	if (crash_occurred()) {
		my $input_b64 = encode_base64($str, '');
		my $s = "crash_input:\n$input_b64\n";
		print $s;
		$log->print($s);
		return;
	}
	goto &main_loop;
}

MAIN: {
	$log = IO::File->new($LOGNAME, 'a') or fatal('open log');
	fatal('old core file exists') if (crash_occurred());
	close(*STDERR);
	pledge('stdio rpath proc exec');
	main_loop('/usr/bin/lex', '-');
	$log->close();
	exit(0);
}
