#!/usr/bin/env perl

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use warnings;

use Getopt::Std qw(getopts);
use MIDI qw();

use constant MICROSECONDS_PER_MINUTE => 60_000_000;
use constant BPM_MAX => 300;
use constant BPM_MIN => 10;

sub usage {
    die("usage: $0 -i FILE [ -r | -b BPM ]\n");
}

sub bad_track {
    die("failed to process midi track data\n");
}

sub bpm2midi {
    my $bpm = shift;

    die('divide by zero') if ($bpm == 0);
    return int(MICROSECONDS_PER_MINUTE / $bpm);
}

sub opts {
    my %opt;
    getopts('b:i:r', \%opt);
    unless ($opt{'i'}) {
        warn("input file expected (-i)\n");
        usage();
    }
    die('file not found') unless (-e $opt{'i'});

    if (! $opt{'b'} && ! $opt{'r'}) {
        warn("expected either -b or -r\n");
        usage();
    }
    if ($opt{'b'} && $opt{'r'}) {
        warn("ignoring option -r\n");
        delete $opt{'r'};
    }
    if ($opt{'b'}) {
        if ($opt{'b'} =~ m/\D/) {
            warn("option -b must be a positive integer\n");
            usage();
        }
        if ($opt{'b'} < BPM_MIN) {
            warn("using minimum bpm of " . BPM_MIN . "\n");
            $opt{'b'} = BPM_MIN;
        } elsif ($opt{'b'} > BPM_MAX) {
            warn("using maximum bpm of " . BPM_MAX . "\n");
            $opt{'b'} = BPM_MAX;
        }
    }

    my @name = split(/\./, $opt{'i'});
    $opt{'o'} = sprintf('%s-%s.mid', $name[0], $opt{'b'} || 'notempo');
    die($opt{'o'} . ' exists') if (-e $opt{'o'});

    return %opt;
}

sub get_tempo_events {
    my $events = shift;

    my @tempo_events;
    for my $e (@$events) {
        die('bad event') unless (ref($e) eq 'ARRAY');
        if ($e->[0] eq 'set_tempo') {
            push @tempo_events, $e;
        }
    }
    return @tempo_events;
}

sub open_midi_file {
    my $path = shift;

    my $midi = MIDI::Opus->new({ 'from_file' => $path });
    bad_track() unless (exists $midi->{'tracks'});
    my $tracks = $midi->{'tracks'};
    bad_track() if (ref($tracks) ne 'ARRAY' || scalar(@$tracks) == 0);

    for (@$tracks) {
        unless (exists $_->{'events'} && ref($_->{'events'}) eq 'ARRAY') {
            bad_track();
        }
    }
    return $midi;
}

sub set_tempo {
    my ($midi, $new_bpm) = @_;

    my @tempo_events;
    for (@{ $midi->{'tracks'} }) {
        push @tempo_events, get_tempo_events($_->{'events'});
    }

    my $timeval = bpm2midi($new_bpm);
    if (scalar(@tempo_events) == 0) {
        unshift @{ $midi->{'tracks'}->[0]->{'events'} },
            ['set_tempo', 0, $timeval];
        print "inserted SetTempo\n";
    } else {
        for (@tempo_events) {
            $_->[2] = $timeval;
            print "modified SetTempo\n";
        }
    }
    return;
}

sub remove_tempo {
    my $midi = shift;

    my $found = 0;
    for my $t (@{ $midi->{'tracks'} }) {
        my $events = $t->{'events'};
        my @to_splice;
        my $n = scalar(@$events) - 1;
        for (0 .. $n) {
            if ($events->[$_]->[0] eq 'set_tempo') {
                push @to_splice, $_;
                $found = 1;
                print "removed SetTempo\n";
            }
        }

        my @sorted = sort { $a <=> $b } @to_splice;
        for (reverse @sorted) {
            splice @$events, $_, 1;
        }
    }
    return $found;
}

MAIN: {
    my %opt = opts();
    my $midi = open_midi_file($opt{'i'});
    if ($opt{'b'}) {
        set_tempo($midi, $opt{'b'});
    } else {
        my $modified = remove_tempo($midi);
        unless ($modified) {
            print "no change needed; suppressing output\n";
            exit(0);
        }
    }
    $midi->write_to_file($opt{'o'});
    printf("wrote file %s\n", $opt{'o'});
    exit(0);
}

__END__

=pod

=head1 NAME

midi_tempo - manipulate midi file tempo

=head1 SYNOPSIS

    midi_tempo -i FILE -b NUMBER    # set new tempo
    midi_tempo -i FILE -r           # remove tempo

=head1 DESCRIPTION

Change values of 'Set Tempo' events in a MIDI file.
The number provided to option -b is the desired
tempo; it is interpreted as beats per minute (bpm).

A compliant MIDI file can include zero or more
'Set Tempo' events.
Option -r removes all 'Set Tempo' events.
Option -b guarantees the output will contain
at least one 'Set Tempo' event at the specified bpm.

The input file is not modified.

=head1 SEE ALSO

MIDI specification details at
L<http://cs.fit.edu/~ryan/cse4051/projects/midi/midi.html>.

=cut
