#!/usr/bin/env perl

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use warnings;

use IO::File qw();
use MIDI qw();

my $USAGE_TEXT = qq{usage:
 $0 midifile stegfile outfile   # hide
 $0 midifile                    # seek
};

sub usage {
    die($USAGE_TEXT);
}

sub open_midi {
    my $path = shift;

    my $midi = MIDI::Opus->new({ 'from_file' => $path });
    unless ($midi) {
        die("$path: failed to load\n");
    }
    my $tracks = $midi->{'tracks'};
    if (ref($tracks) ne 'ARRAY') {
        die("$path: invalid track data\n");
    }
    if (scalar(@$tracks) == 0) {
        die("$path: midi file contains no tracks\n");
    }
    my $events = $tracks->[0]->{'events'};
    if (ref($events) ne 'ARRAY') {
        die("$path: invalid event data\n");
    }
    return $midi;
}

sub insert_events {
    my ($midi, $newevents) = @_;

    my $events = $midi->{'tracks'}->[0]->{'events'};
    push @$events, @$newevents;
    return;
}

sub encode_events {
    my $path = shift;

    my $fh = IO::File->new($path, 'r')
        or die("$path: failed to load\n");
    my @events;
    push @events, [ 'note_off', 88, 0, 0 ];

    my $buf;
    while ($fh->read($buf, 1)) {
        my $c = ord($buf);
        my $note = $c & 0xf;
        my $velo = ($c & 0xf0) >> 4;
        push @events, [ 'note_off', 1, $note, $velo];
    }
    push @events, [ 'note_off', 88, 0, 0 ];
    return [ @events ];
}

sub decode_events {
    my $midi = shift;

    my $excited = 0;
    my $events = $midi->{'tracks'}->[0]->{'events'};

    for (@$events) {
        if ($_->[0] eq 'note_off' && $_->[1] == 88 &&
            $_->[2] == 0 && $_->[3] == 0) {
            $excited = ($excited + 1) & 1;
            next;
        }
        if ($excited && $_->[0] eq 'note_off') {
            my $c = ($_->[3] << 4) | $_->[2];
            print chr($c);
        }
    }
    return;
}

MAIN: {
    my $n = scalar(@ARGV);
    if ($n != 1 && $n != 3) {
        warn("wrong number of arguments\n");
        usage();
    }
    for (@ARGV) {
        if (! $_ || length($_) == 0) {
            warn("bad argument: '$_'\n");
            usage();
        }
    }
    my $midifile = $ARGV[0];
    unless (-e $midifile) {
        die("file not found: '$midifile'\n");
    }
    my $midi = open_midi($midifile);

    if ($n == 3) {
        my $stegfile = $ARGV[1];
        unless (-e $stegfile) {
            die("file not found: '$stegfile'\n");
        }
        my $outfile = $ARGV[2];
        if (-e $outfile) {
            die("file '$outfile' exists\n");
        }
        insert_events($midi, encode_events($stegfile));
        $midi->write_to_file($outfile);
    } else {
        decode_events($midi);
    }
    exit(0);
}

__END__

=pod

=head1 NAME

midi_steg - hide a file in a midi file

=head1 SYNOPSIS

    # hide stegfile in midifile
    midi_steg midifile stegfile outfile

    # unpack hidden file from midifile
    midi_steg midifile > outfile

=head1 DESCRIPTION

This tool encodes the file 'stegfile' as a set
of MIDI 'Note Off' events, then adds these events
to the original MIDI file. The encoding method is
8-bit clean; 8 bits of input are encoded in each
'Note Off' event. This means 'stegfile' does not
require base64 or other 7-bit formatting.

The 'Note Off' event type was chosen because it is
supported by all MIDI implementations.
File sizes are not limited; however, MIDI files
are generally small. A 10K MIDI file would not
raise suspicion but a 100MB file would.

=head1 BUGS

Certain MIDI software might behave strangely
when interpreting the 'Note Off' events.

=head1 SEE ALSO

MIDI specification details at
L<http://cs.fit.edu/~ryan/cse4051/projects/midi/midi.html>.

=cut
