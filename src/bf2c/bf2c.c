/*
 * Copyright (c) 2017-2019 Michael Mikonos <mb@ii.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char bf_code_chars[] = "><+-.,[]";

int unget_char = -1;

int
bf_getc(void)
{
	int c;

	for (;;) {
		if (unget_char == -1)
			c = fgetc(stdin);
		else {
			c = unget_char;
			unget_char = -1;
		}

		if (c == EOF)
			break;
		if (strchr(bf_code_chars, c) != NULL)
			break;
	}
	return c;
}

void
bf_ungetc(int c)
{
	unget_char = c;
}

int
get_incr(void)
{
	int c, i = 0;

	for (;;) {
		c = bf_getc();
		if (c == '+') {
			i++;
		} else if (c == '-') {
			i--;
		} else {
			bf_ungetc(c);
			return i;
		}
	}	
}

int
get_shift(void)
{
	int c, i = 0;

	for (;;) {
		c = bf_getc();
		if (c == '>') {
			i++;
		} else if (c == '<') {
			i--;
		} else {
			bf_ungetc(c);
			return i;
		}
	}	
}

void
emit_indent(int depth)
{
	while (depth-- > 0)
		putchar('\t');
}

void
emit_shift(int val)
{
	printf("%s(%d);\n", val > 0 ? "RIGHT" : "LEFT", abs(val));
}

void
emit_incr(int val)
{
	printf("%s(%d);\n", val > 0 ? "ADD" : "SUB", abs(val));
}

void
header(int use_buffered_io)
{
	puts("#include <stdio.h>");
	puts("#include <stdlib.h>");
	if (!use_buffered_io)
		puts("#include <unistd.h>");
	fputc('\n', stdout);

	puts("#define NCELLS 30000");
	puts("#define IS_TRUE() (cells[cell_idx] != 0)");
	puts("#define RIGHT(x) do { cell_idx = (cell_idx + (x)) % NCELLS; } while (0)");
	puts("#define LEFT(x) do { cell_idx = (cell_idx - (x)) % NCELLS; } while (0)");
	puts("#define ADD(x) do { cells[cell_idx] = (cells[cell_idx] + (x)) & 0xff; } while (0)");
	puts("#define SUB(x) do { cells[cell_idx] = (cells[cell_idx] - (x)) & 0xff; } while (0)");
	if (use_buffered_io) {
		puts("#define PUTC() do { fputc(cells[cell_idx], stdout); } while (0)");
		puts("#define GETC() do { cells[cell_idx] = fgetc(stdin); } while (0)");
	} else {
		puts("#define PUTC() do { write(STDOUT_FILENO, &cells[cell_idx], 1); } while (0)");
		puts("#define GETC() do { read(STDIN_FILENO, &cells[cell_idx], 1); } while (0)");
	}
	fputs("\nint\n", stdout);
	puts("main(int argc, char *argv[])");
	puts("{");
	puts("\tint cell_idx = 0;");
	puts("\tunsigned char *cells = NULL;");
	fputc('\n', stdout);
	puts("\tcells = calloc(1, NCELLS);");
	puts("\tif (cells == NULL) {");
	puts("\t\tfprintf(stderr, \"calloc failed\\n\");");
	puts("\t\treturn (-1);");
	puts("\t}");
	fputc('\n', stdout);
}

void
footer(void)
{
	puts("\tfree(cells);");
	puts("\treturn (0);");
	puts("}");
}

int
main(int argc, char *argv[])
{
	int c, i, buffered_io, lookahead, depth = 1;

	buffered_io = (argc >= 2 && strcmp(argv[1], "-u") == 0) ? 0 : 1;
	header(buffered_io);

	for (;;) {
		c = bf_getc();
		if (c == EOF)
			break;

		switch (c) {
		case '+':
		case '-':
			bf_ungetc(c);
			i = get_incr();
			if (i == 0)
				break;
			emit_indent(depth);
			emit_incr(i);
			break;
		case '>':
		case '<':
			bf_ungetc(c);
			i = get_shift();
			if (i == 0)
				break;
			emit_indent(depth);
			emit_shift(i);
			break;
		case '.':
			emit_indent(depth);
			fputs("PUTC();\n", stdout);
			break;
		case ',':
			emit_indent(depth);
			fputs("GETC();\n", stdout);
			break;
		case '[':
			lookahead = bf_getc();
			if (lookahead == ']')
				break;
			bf_ungetc(lookahead);
			emit_indent(depth);
			fputs("while (IS_TRUE()) {\n", stdout);
			depth++;
			break;
		case ']':
			depth--;
			if (depth == 0)
				errx(1, "] found with no leading [");
			emit_indent(depth);
			printf("}\n");
			break;
		}
	}

	if (depth != 1)
		errx(1, "unmatched [] at end of input");
	footer();
	return 0;
}
