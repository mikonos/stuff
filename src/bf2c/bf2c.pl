#!/usr/bin/env perl

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use warnings;

use Getopt::Std qw(getopts);

my $BADCHAR = qr([^\[\]\<\>\+\-\.,]);

my $indent = "\t";

my %ACTIONS = (
	'<' => sub { printf("%sLEFT(%d);\n",  $indent, $_[0]) },
	'>' => sub { printf("%sRIGHT(%d);\n", $indent, $_[0]) },
	'+' => sub { printf("%sADD(%d);\n",   $indent, $_[0]) },
	'-' => sub { printf("%sSUB(%d);\n",   $indent, $_[0]) },
	'.' => sub { printf("%sPUTC();\n", $indent) },
	',' => sub { printf("%sGETC();\n", $indent) },

	'[' => sub {
		printf("%swhile (IS_TRUE()) {\n", $indent);
		$indent .= "\t";
	},

	']' => sub {
		die('] found with no leading [') if ($indent eq "\t");
		chop($indent);
		printf("%s}\n", $indent);
	},
);

sub header {
	my %opt;
	getopts('u', \%opt);

	printf("#include <stdio.h>\n");
	printf("#include <string.h>\n");
	printf($opt{'u'} ? "#include <unistd.h>\n\n" : "\n");
	printf("#define NCELLS 30000\n");
	printf("unsigned char cells[NCELLS];\n");
	printf("int cell_idx = 0;\n");
	printf("#define IS_TRUE() (cells[cell_idx] != 0)\n");
	printf("#define RIGHT(x) do { cell_idx = (cell_idx + (x)) %% NCELLS; } while (0)\n");
	printf("#define LEFT(x) do { cell_idx = (cell_idx - (x)) %% NCELLS; } while (0)\n");
	printf("#define ADD(x) do { cells[cell_idx] = (cells[cell_idx] + (x)) & 0xff; } while (0)\n");
	printf("#define SUB(x) do { cells[cell_idx] = (cells[cell_idx] - (x)) & 0xff; } while (0)\n");
	if ($opt{'u'}) {
		printf("#define PUTC() do { write(STDOUT_FILENO, &cells[cell_idx], 1); } while (0)\n");
		printf("#define GETC() do { read(STDIN_FILENO, &cells[cell_idx], 1); } while (0)\n\n");
	} else {
		printf("#define PUTC() do { fputc(cells[cell_idx], stdout); } while (0)\n");
		printf("#define GETC() do { cells[cell_idx] = fgetc(stdin); } while (0)\n\n");
	}
	printf("int\n");
	printf("main(int argc, char *argv[])\n");
	printf("{\n");
	printf("\tmemset(cells, 0, sizeof(cells));\n\n");
}

sub footer {
	printf("\treturn (0);\n");
	printf("}\n");
}

MAIN: {
	my @lines = readline(*STDIN);
	my $prog = join('', @lines);
	$prog =~ s/$BADCHAR//g;

	header();
	while (length($prog) > 0 && $prog =~ s/^(\++|\-+|\<+|\>+|\[|\]|\.|,)//) {
		my $match = $1;
		my $len = length($match);
		my $op = substr($match, 0, 1);
		my $func = $ACTIONS{$op};
		$func->($len);
	}
	die('unmatched [] at end of input') if ($indent ne "\t");
	footer();
}
