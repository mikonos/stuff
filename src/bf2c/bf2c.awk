#!/usr/bin/awk -f

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

BEGIN {
	use_buffered_io = ENVIRON["BF2C_UNBUF"] ? 0 : 1;
	indent = "\t";
	prog = "";

	printf("#include <stdio.h>\n");
	printf("#include <string.h>\n");
	printf(use_buffered_io ? "\n" : "#include <unistd.h>\n\n");
	printf("#define NCELLS 30000\n");
	printf("unsigned char cells[NCELLS];\n");
	printf("int cell_idx = 0;\n");
	printf("#define IS_TRUE() (cells[cell_idx] != 0)\n");
	printf("#define RIGHT(x) do { cell_idx = (cell_idx + (x)) %% NCELLS; } while (0)\n");
	printf("#define LEFT(x) do { cell_idx = (cell_idx - (x)) %% NCELLS; } while (0)\n");
	printf("#define ADD(x) do { cells[cell_idx] = (cells[cell_idx] + (x)) & 0xff; } while (0)\n");
	printf("#define SUB(x) do { cells[cell_idx] = (cells[cell_idx] - (x)) & 0xff; } while (0)\n");
	if (use_buffered_io) {
		printf("#define PUTC() do { fputc(cells[cell_idx], stdout); } while (0)\n");
		printf("#define GETC() do { cells[cell_idx] = fgetc(stdin); } while (0)\n\n");
	}
	else {
		printf("#define PUTC() do { write(STDOUT_FILENO, &cells[cell_idx], 1); } while (0)\n");
		printf("#define GETC() do { read(STDIN_FILENO, &cells[cell_idx], 1); } while (0)\n\n");
	}
	printf("int\n");
	printf("main(int argc, char *argv[])\n");
	printf("{\n");
	printf("\tmemset(cells, 0, sizeof(cells));\n\n");
}

{
	gsub(/[^\[\]\.\-+,<>]/, ""); # strip non-code characters from $0
	prog = prog $0;
	#printf("LINE(%s)\n", $0);
}

END {
	while (match(prog, /^(\-+|\++|<+|>+|\[|\]|,|\.)/)) {
		len = RLENGTH;
		op = substr(prog, 1, 1);
		prog = substr(prog, len + 1); # offset starts from 1
		#printf("op(%s) capture(%s) len(%d)\n", op, capture[0], len);
		#printf("LINE(%s)\n", $0);

		if (op == "<") {
			printf("%sLEFT(%d);\n", indent, len);
		}
		else if (op == ">") {
			printf("%sRIGHT(%d);\n", indent, len);
		}
		else if (op == "+") {
			printf("%sADD(%d);\n", indent, len);
		}
		else if (op == "-") {
			printf("%sSUB(%d);\n", indent, len);
		}
		else if (op == ".") {
			printf("%sPUTC();\n", indent);
		}
		else if (op == ",") {
			printf("%sGETC();\n", indent);
		}
		else if (op == "[") {
			printf("%swhile (IS_TRUE()) {\n", indent);
			indent = indent "\t";
		}
		else if (op == "]") {
			if (indent == "\t") {
				printf("ERROR: ] found with no leading [\n");
				exit(1);
			}
			indent = substr(indent, 2);
			printf("%s}\n", indent);
		}
		else {
			printf("ERROR: unexpected (%s)\n", op);
			exit(1);
		}
	}

	if (depth != 0) {
		printf("ERROR: unmatched [] at end of input\n");
		exit(1);
	}
	printf("\treturn (0);\n");
	printf("}\n");
}
