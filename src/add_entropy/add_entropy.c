/*
 * Copyright (c) 2017-2019 Michael Mikonos <mb@ii.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define HEADER_SZ 6
#define PREFIX_SZ 4

/* -f and -s options use same number range */
#define ARG_MAX 255
#define ARG_MIN 1

void
encode(int fd_in, int fd_out, int freq, int size)
{
    char buf[ARG_MAX];
    ssize_t n;

    /* header */
    arc4random_buf(buf, PREFIX_SZ);
    buf[PREFIX_SZ] = (uint8_t) freq;
    buf[PREFIX_SZ + 1] = (uint8_t) size;
    n = write(fd_out, buf, HEADER_SZ);
    if (n != HEADER_SZ)
        errx(1, "write out header");

    do {
        arc4random_buf(buf, size);
        n = write(fd_out, buf, size);
        if (n != size)
            errx(1, "write out random");
        n = read(fd_in, buf, freq);
        if (n == -1)
            errx(1, "read in data");
        if (n > 0)
            write(fd_out, buf, n);
    } while (n == freq);
}

void
decode(int fd_in, int fd_out)
{
    char buf[ARG_MAX];
    uint8_t freq, size;
    ssize_t n;

    /* header */
    n = read(fd_in, buf, HEADER_SZ);
    if (n != HEADER_SZ)
        errx(1, "read in header");
    freq = buf[4];
    size = buf[5];

    for (;;) {
        n = read(fd_in, buf, size);
        if (n == 0)
            break;
        if (n != size)
            errx(1, "read in random");
        n = read(fd_in, buf, freq);
        if (n == 0)
            break;
        if (n == -1)
            errx(1, "read in data");
        write(fd_out, buf, n);
    }
}

int
main(int argc, char **argv)
{
    int ch, enc = 1, freq = 0, size = 0;

    if (pledge("stdio", NULL) == -1)
        err(1, "pledge");

    while ((ch = getopt(argc, argv, "def:s:")) != -1) {
        switch (ch) {
        case 'd':
            enc = 0;
            break;
        case 'e':
            enc = 1;
            break;
        case 'f':
            freq = atoi(optarg);
            break;
        case 's':
            size = atoi(optarg);
            break;
        default:
            break;
        }
    }

    if (size > ARG_MAX)
        size = ARG_MAX;
    else if (size < ARG_MIN)
        size = ARG_MIN;

    if (freq > ARG_MAX)
        freq = ARG_MAX;
    else if (freq < ARG_MIN)
        freq = ARG_MIN;

    if (enc)
        encode(STDIN_FILENO, STDOUT_FILENO, freq, size);
    else
        decode(STDIN_FILENO, STDOUT_FILENO); /* ignore -f & -s */

    return 0;
}
