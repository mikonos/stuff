#!/usr/bin/env perl

# Copyright (c) 2017-2019 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use bytes;
use integer;
use strict;
use warnings;

use Getopt::Std qw(getopts);

use Const::Fast qw(const);
use Unix::OpenBSD::Random qw(arc4random_buf);
use Unix::Pledge qw(pledge);

const my $HEADER_SZ => 6;
const my $PREFIX_SZ => 4;
const my $ARG_MAX => 255;
const my $ARG_MIN => 1;

const my $USAGE_TEXT => qq{usage: $0 [OPTIONS]
Options:
 -e       Encode; add entropy to input (default)
 -d       Decode; remove entropy from input
 -s NUM   Size of entropy blocks in bytes
 -f NUM   Size of input blocks in bytes
};

sub usage {
    die($USAGE_TEXT);
}

sub encode {
    my ($in, $out, $freq, $size) = @_;

    # header
    my $buf = arc4random_buf($PREFIX_SZ);
    if (length($buf) != $PREFIX_SZ) {
        die('arc4random_buf');
    }
    $buf .= pack('C', $freq);
    $buf .= pack('C', $size);
    my $s = syswrite($out, $buf, $HEADER_SZ);
    if ($s != $HEADER_SZ) {
        die('write out header');
    }

    do {
        $buf = arc4random_buf($size);
        if (length($buf) != $size) {
            die('arc4random_buf');
        }
        $s = syswrite($out, $buf, $size);
        if ($s != $size) {
            die('write out random');
        }
        $s = sysread($in, $buf, $freq);
        if (!defined $s) {
            die('read in data');
        }
        if ($s > 0) {
            syswrite($out, $buf, $s);
        }
    } while ($s == $freq);
    return;
}

sub decode {
    my ($in, $out) = @_;

    # header
    my $buf;
    my $s = sysread($in, $buf, $HEADER_SZ);
    if ($s != $HEADER_SZ) {
        die('read in header');
    }
    my $freq = unpack('C', substr($buf, 4 , 1));
    my $size = unpack('C', substr($buf, 5 , 1));

    for (;;) {
        $s = sysread($in, $buf, $size);
        if ($s == 0) {
            last;
        }
        if ($s != $size) {
            die("read in random");
        }
        $s = sysread($in, $buf, $freq);
        if (!defined($s)) {
            die('read in data');
        }
        if ($s == 0) {
            last;
        }
        syswrite($out, $buf, $s);
    }
    return;
}

MAIN: {
    pledge('stdio');

    my %opt;
    getopts('def:s:', \%opt) or usage();
    if ($opt{'d'} && $opt{'e'}) {
        warn("options -d and -e are mutually exclusive\n");
        usage();
    }
    my $encode = ($opt{'d'}) ? 0 : 1;

    my $size = int($opt{'s'} || 0);
    if ($size =~ m/\D/) {
        warn("-s option expects a number\n");
        usage();
    }
    my $freq = int($opt{'f'} || 0);
    if ($freq =~ m/\D/) {
        warn("-f option expects a number\n");
        usage();
    }

    if ($size > $ARG_MAX) {
        $size = $ARG_MAX;
    } elsif ($size < $ARG_MIN) {
        $size = $ARG_MIN;
    }
    if ($freq > $ARG_MAX) {
        $freq = $ARG_MAX;
    } elsif ($freq < $ARG_MIN) {
        $freq = $ARG_MIN;
    }

    if ($encode) {
        encode(*STDIN, *STDOUT, $freq, $size);
    } else {
        decode(*STDIN, *STDOUT);
    }
    exit(0);
}

__END__

=pod

=head1 NAME

add_entropy - interleave random data in a file

=head1 SYNOPSIS

    add_entropy -e -f 4 -s 3 < FILE_IN > FILE_OUT
    add_entropy -d < FILE_IN > FILE_OUT

=head1 DESCRIPTION

Generally the more entropy in an encrypted file
the more difficult to observe patterns. Such
patterns can lead to an encryption system being
vulnerable to certain attacks.

This tool divides an input file into blocks of 'f'
bytes. Each block is prepended with 's' bytes of
entropy.
Both 'f' and 's' must be in the range of 1-255.

The values of 'f' and 's' are encoded in the output.
Options -f and -s are used only when encoding;
they are ignored if option -d is provided.

As a result of adding random bytes the output is
always larger than the input (when encoding).
To grow the file only a little use a high value of
'f' and a low value of 's'.
For maximum entropy use f=1 and s>1; however, the
input will grow by 100% or more.

=head1 BUGS

Certain methods of encryption are better at mixing
entropy into the message than others (think CFB
versus OFB).

=cut
