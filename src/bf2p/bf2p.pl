#!/usr/bin/env perl

# Copyright (c) 2017 Michael Mikonos <mb@ii.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use strict;
use warnings;

use Getopt::Std qw(getopts);

my $BADCHAR = qr([^\[\]\<\>\+\-\.,]);

my $indent = "\t";

my %ACTIONS = (
	'<' => sub { printf("%sLEFT(%d);\n",  $indent, $_[0]) },
	'>' => sub { printf("%sRIGHT(%d);\n", $indent, $_[0]) },
	'+' => sub { printf("%sADD(%d);\n",   $indent, $_[0]) },
	'-' => sub { printf("%sSUB(%d);\n",   $indent, $_[0]) },
	'.' => sub { printf("%sPUTC();\n", $indent) },
	',' => sub { printf("%sGETC();\n", $indent) },

	'[' => sub {
		printf("%swhile (IS_TRUE()) {\n", $indent);
		$indent .= "\t";
	},

	']' => sub {
		die('] found with no leading [') if ($indent eq "\t");
		chop($indent);
		printf("%s}\n", $indent);
	},
);

sub header {
	my %opt;
	getopts('u', \%opt);

	print("#!/usr/bin/env perl\n\n");
	print("use strict;\n");
	print("use warnings;\n\n");
	print("my \$NCELLS = 30_000;\n");
	print("my \@cells = map { 0 } (1 .. \$NCELLS);\n");
	print("my \$cell_idx = 0;\n\n");
	print("sub IS_TRUE { return \$cells[\$cell_idx] != 0; }\n");
	print("sub LEFT    { \$cell_idx = (\$cell_idx - \$_[0]) % \$NCELLS; }\n");
	print("sub RIGHT   { \$cell_idx = (\$cell_idx + \$_[0]) % \$NCELLS; }\n");
	print("sub ADD     { \$cells[\$cell_idx] = (\$cells[\$cell_idx] + \$_[0]) & 0xff; }\n");
	print("sub SUB     { \$cells[\$cell_idx] = (\$cells[\$cell_idx] - \$_[0]) & 0xff; }\n");
	if ($opt{'u'}) {
		print("sub PUTC    { syswrite(*STDOUT, chr(\$cells[\$cell_idx]), 1); }\n");
		print("sub GETC    { my \$c; sysread(*STDIN, \$c, 1); \$cells[\$cell_idx] = ord(\$c); }\n");
	} else {
		print("sub PUTC    { print chr(\$cells[\$cell_idx]); }\n");
		print("sub GETC    { my \$c; read(*STDIN, \$c, 1); \$cells[\$cell_idx] = ord(\$c); }\n");
	}
	print("\n{\n");
}

sub footer { print "}\n"; }

MAIN: {
	my @lines = readline(*STDIN);
	my $prog = join('', @lines);
	$prog =~ s/$BADCHAR//g;

	header();
	while (length($prog) > 0 && $prog =~ s/^(\++|\-+|\<+|\>+|\[|\]|\.|,)//) {
		my $match = $1;
		my $len = length($match);
		my $op = substr($match, 0, 1);
		my $func = $ACTIONS{$op};
		$func->($len);
	}
	die('unmatched [] at end of input') if ($indent ne "\t");
	footer();
}
