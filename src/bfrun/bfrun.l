%{
/*
 * Copyright (c) 2017-2019 Michael Mikonos <mb@ii.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PROGMAX 30000

#define NCELLS 30000
unsigned char cells[NCELLS];
int cell_idx = 0;

enum BF_TOKEN {
	BF_LEFT = 1,    /* < */
	BF_RIGHT = 2,   /* > */
	BF_INCR = 3,    /* + */
	BF_DECR = 4,    /* - */
	BF_PUTC = 5,    /* . */
	BF_GETC = 6,    /* , */
	BF_WHILE = 7,   /* [ */
	BF_ENDWHILE = 8 /* ] */
};

enum vm_optype {
	VM_LEFT = 1,
	VM_RIGHT = 2,
	VM_ADD = 3,
	VM_SUB = 4,
	VM_PUTC = 5,
	VM_GETC = 6,
	VM_JUMP = 7,
	VM_JUMPZ = 8,
};

#define NJUMPS 1000
#define NAMELEN 5
struct jumplabel {
	int addr;
	char name[NAMELEN];
};

struct vm_op {
	enum vm_optype id;
	char name[NAMELEN];
	int val;
};

#define MAXDEPTH 1000
short labels[MAXDEPTH];
int depth = 0;
int loopcounter = 0;

%}

%option noyywrap

%%

"<"+ { return (BF_LEFT); }
">"+ { return (BF_RIGHT); }
"+"+ { return (BF_INCR); }
"-"+ { return (BF_DECR); }
"." { return (BF_PUTC); }
"," { return (BF_GETC); }
"[" { return (BF_WHILE); }
"]" { return (BF_ENDWHILE); }
.|\n { } 

%%

struct vm_op *
new_vmop(void)
{
	void *ptr;

	if ((ptr = malloc(sizeof(struct vm_op))) == NULL)
		errx(1, "failed to allocate memory");
	return ptr;
}

struct jumplabel *
new_jumplabel(void)
{
	void *ptr;

	if ((ptr = malloc(sizeof(struct jumplabel))) == NULL)
		errx(1, "failed to allocate memory");
	return ptr;
}

void
read_char(void)
{
	(void) read(STDIN_FILENO, &cells[cell_idx], 1);
}

void
read_char_stdio(void)
{
	cells[cell_idx] = fgetc(stdin);
}

void
write_char(void)
{
	(void) fputc(cells[cell_idx], stdout);
}

void
write_char_stdio(void)
{
	(void) write(STDOUT_FILENO, &cells[cell_idx], 1);
}

int
main(int argc, char *argv[])
{
	int tok, i, j, progc, jumpc, found_goto;
	struct vm_op *prog[PROGMAX];
	struct jumplabel *jumps[NJUMPS];
	void (*reader)(void), (*writer)(void);

	if (argc >= 2 && strcmp(argv[1], "-u") == 0) {
		reader = read_char_stdio;
		writer = write_char_stdio;
	} else {
		reader = read_char;
		writer = write_char;
	}

	i = j = 0;
	while ((tok = yylex())) {
		if (yyleng < 1)
			errx(1, "zero-length token encountered");
#ifdef DEBUG
		printf("\tyytext(%s) yyleng(%d)\n", yytext, (int)yyleng);
#endif

		if (i >= PROGMAX)
			errx(1, "program is too big");
		prog[i] = new_vmop();
		switch (tok) {
		case BF_LEFT:
			prog[i]->id = VM_LEFT;
			prog[i]->val = yyleng;
			break;
		case BF_RIGHT:
			prog[i]->id = VM_RIGHT;
			prog[i]->val = yyleng;
			break;
		case BF_INCR:
			prog[i]->id = VM_ADD;
			prog[i]->val = yyleng;
			break;
		case BF_DECR:
			prog[i]->id = VM_SUB;
			prog[i]->val = yyleng;
			break;
		case BF_PUTC:
			prog[i]->id = VM_PUTC;
			break;
		case BF_GETC:
			prog[i]->id = VM_GETC;
			break;
		case BF_WHILE:
			if (j >= NJUMPS)
				errx(1, "too many goto labels");
			if (depth >= MAXDEPTH)
				errx(1, "too many levels of loop nesting");
			loopcounter++;
			labels[depth] = loopcounter;
			depth++;

			prog[i]->id = VM_JUMPZ;
			snprintf(prog[i]->name, NAMELEN, "E%d", loopcounter);
#ifdef DEBUG
			printf("jumpz %s inserted at PC=%d\n", prog[i]->name, i);
#endif

			jumps[j] = new_jumplabel();
			jumps[j]->addr = i;
			snprintf(jumps[j]->name, NAMELEN, "B%d", loopcounter);
			j++;
			break;
		case BF_ENDWHILE:
			if (j >= NJUMPS)
				errx(1, "too many goto labels");
			if (depth == 0)
				errx(1, "] found with no leading [");
			depth--;

			prog[i]->id = VM_JUMP;
			snprintf(prog[i]->name, NAMELEN, "B%d", labels[depth]);
#ifdef DEBUG
			printf("jump %s inserted at PC=%d\n", prog[i]->name, i);
#endif

			jumps[j] = new_jumplabel();
			jumps[j]->addr = i + 1;
			snprintf(jumps[j]->name, NAMELEN, "E%d", labels[depth]);
			j++;
			break;
		default:
			errx(1, "unexpected input");
		}
		i++;
	}
	progc = i;
	jumpc = j;
#ifdef DEBUG
	printf("progc=%d jumpc=%d\n", progc, jumpc);
#endif

	if (depth != 0)
		errx(1, "unmatched [] at end of input");

	/* map jump label names to program addresses */
	for (i = 0; i < progc; i++) {
		if (prog[i]->id == VM_JUMP || prog[i]->id == VM_JUMPZ) {
			found_goto = 0;
#ifdef DEBUG
			printf("saw jump op=%d goto=%s at PC=%d\n", prog[i]->id, prog[i]->name, i);
#endif
			for (j = 0; j < jumpc; j++) {
				if (strcmp(prog[i]->name, jumps[j]->name) == 0) {
					found_goto = 1;
					prog[i]->val = jumps[j]->addr;
#ifdef DEBUG
					printf("label %s is at PC=%d\n", jumps[j]->name, prog[i]->val);
#endif
					break;
				}
			}
			if (!found_goto)
				errx(1, "unable to resolve address for '%s'", prog[i]->name);
		}
	}

	memset(cells, 0, sizeof(cells));

	i = 0;
	while (i < progc) {
		switch (prog[i]->id) {
		case VM_LEFT:
			cell_idx = (cell_idx - prog[i]->val) % NCELLS;
			break;
		case VM_RIGHT:
			cell_idx = (cell_idx + prog[i]->val) % NCELLS;
			break;
		case VM_ADD:
			cells[cell_idx] = (cells[cell_idx] + prog[i]->val) & 0xff;
			break;
		case VM_SUB:
			cells[cell_idx] = (cells[cell_idx] - prog[i]->val) & 0xff;
			break;
		case VM_PUTC:
			writer();
			break;
		case VM_GETC:
			reader();
			break;
		case VM_JUMP:
			i = prog[i]->val;
			continue;
		case VM_JUMPZ:
			if (cells[cell_idx] == 0) {
				i = prog[i]->val;
				continue;
			}
			break;
		default:
			errx(1, "unknown opcode (%d)", prog[i]->id);
		}

		i++;
	}
	
	return (0);
}
